/*
Copyright 2019 Tyler French.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package client

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
    //Replace with vcd version
	"sigs.k8s.io/controller-runtime/pkg/client"

    //needed govcd librires 
    "github.com/vmware/go-vcloud-director/v2/govcd"
)

//go:generate mockgen -source=./client.go -destination=./mock/client_generated.go -package=mock

const (
	// VcdUserName is secret key conntaining VCD Username
	VcdUserName = "vcd_user_name"
	// VcdPassword is secret key containing VCD Password
	VcdPassword = "vcd_password"
    // VcdOrg is secret key containing VCD Org
    VcdOrg = "vcd_org"
    // VcdInsecure is secret key containing VCD Insecure
    VcdInsecure = "vcd_insecure"
)

// VcdClientBuilderFuncType is function type for building vcd client
type VcdClientBuilderFuncType func(client client.Client, secretName, namespace, region string) (Client, error)

// Client is a wrapper object for actual VCD SDK clients to allow for easier testing.
type Client interface {
	DescribeCatalog(*vcd.DescribeCatalogInput) (*vcd.DescribeCatalogOutput, error)
	DescribeOrg(*vcd.DescribeOrgInput) (*vcd.DescribeOrgOutput, error)
	DescribeVdc(*vcd.DescribeVdcInput) (*vcd.DescribeVdcOutput, error)
	DescribeEdgeGateway(*vcd.DescribeEdgeGatewayInput) (*vcd.DescribeEdgeGatewayOutput, error)
    //Might be more to come on this ^^
	DescribeOrgVdcNetwork(*vcd.DescribeOrgVdcNetworkInput) (*vcd.DescribeOrgVdcNetworkOutput, error)
	DescribeNetworkPool(*vcd.DescribeNetworkPoolInput) (*vcd.DescribeNetworkPoolOutput, error)
    //Might be more to come on this ^^
	DescribeProviderVdc(*vcd.DescribeProviderVdcInput) (*vcd.DescribeProviderVdcOutput, error)
	RunVapp(*vcd.RunVappInput) (*vcd.VappReservation, error)
	DescribeVapp(*vcd.DescribeVappInput) (*vcd.DescribeVappOutput, error)
    RunVm(*vcd.RunVmInput) (*vcd.VmReservation, error)
    DescribeVm(*vcd.DescribeVmInput) (*vcd.DescribeVmOutput, error)
    TerminateVapp(*vcd.TerminateVappInput) (*vcd.TerminateVappOutput, error)
    TerminateVm(*vcd.TerminateVmInput) (*vcd.TerminateVmOutput, error)
    DescribeVappStoragePolicy(*vcd.DescribeVappStoragePolicyInput) (*vcd.DescribeVappStoragePolicyOutput, error)

	RegisterVmWithLoadBalancer(*nsx.RegisterVmWithLoadBalancerInput) (*nsx.RegisterVmWithLoadBalancerOutput, error)
	NsxDescribeLoadBalancers(*nsx.DescribeLoadBalancersInput) (*nsx.DescribeLoadBalancersOutput, error)
	NsxDescribeVirtualServers(*nsx.DescribeVirtualServersInput) (*nsx.DescribeVirtualServersOutput, error)
	NsxRegisterServerPool(*nsx.RegisterServerPoolInput) (*nsx.RegisterServerPoolOutput, error)
}

/*

TODO:
    - fix below returns to have the correct values returned from the client or just run the code to get them?
        - Might need wrapper for govcd.client so that I can just wrap these function call below and use more of the base here? Good Idea
*/

type vcdClient struct {
    vcdclient       govcd.Client
}

func (c *vcdClient) DescribeCatalog(input *vcd.DescribeCatalogInput) (*vcd.DescribeCatalogOutput, error) {
	return c.vcdClient.DescribeCatalog(input)
}

func (c *vcdClient) DescribeOrg(input *vcd.DescribeOrgInput) (*vcd.DescribeOrgOutput, error) {
	return c.vcdClient.DescribeOrg(input)
}

func (c *vcdClient) DescribeVdc(input *vcd.DescribeVdcInput) (*vcd.DescribeVdcOutput, error) {
	return c.vcdClient.DescribeVdcs(input)
}

func (c *vcdClient) DescribeEdgeGateway(input *vcd.DescribeEdgeGatewayInput) (*vcd.DescribeEdgeGatewayOutput, error) {
	return c.vcdClient.DescribeEdgeGateway(input)
}

func (c *vcdClient) DescribeOrgVdcNetwork(input *vcd.DescribeOrgVdcNetworkInput) (*vcd.DescribeOrgVdcNetworkOutput, error) {
	return c.vcdClient.DescribeOrgVdcNetwork(input)
}

func (c *vcdClient) DescribeNetworkPool(input *vcd.DescribeNetworkPoolInput) (*vcd.DescribeNetworkPoolOutput, error) {
	return c.vcdClient.DescribeNetworkPool(input)
}

func (c *vcdClient) DescribeProviderVdc(input *vcd.DescribeProviderVdcInput) (*vcd.DescribeProviderVdcOutput, error) {
	return c.vcdClient.DescribeProviderVdc(input)
}

func (c *vcdClient) RunVapp(input *vcd.RunVappInput) (*vcd.VappReservation, error) {
	return c.vcdClient.RunVapp(input)
}

func (c *vcdClient) DescribeVapp(input *vcd.DescribeVappInput) (*vcd.DescribeVappOutput, error) {
	return c.vcdClient.DescribeVapp(input)
}

func (c *vcdClient) RunVm(input *vcd.RunVmInput) (*vcd.VmReservation, error) {
	return c.vcdClient.RunVm(input)
}

func (c *vcdClient) DescribeVm(input *vcd.DescribeVmInput) (*vcd.DescribeVmOutput, error) {
	return c.vcdClient.DescribeVm(input)
}

func (c *vcdClient) TerminateVapp(input *vcd.TerminateVappInput) (*vcd.TerminateVappOutput, error) {
	return c.vcdClient.TerminateVapp(input)
}

func (c *vcdClient) TerminateVm(input *vcd.TerminateVmInput) (*vcd.TerminateVmOutput, error) {
	return c.vcdClient.TerminateVapp(input)
}

func (c *vcdClient) DescribeVappStoragePolicy(input *vcd.DescribeVappStoragePolicyInput) (*vcd.DescribeVappStoragePolicyOutput, error) {
	return c.vcdClient.DescribeVappStoragePolicy(input)
}

// Below might be migrated to *nsxClient.
func (c *vcdClient) RegisterVmWithLoadBalancer(input *nsx.RegisterVmWithLoadBalancerInput) (*nsx.RegisterVmWithLoadBalancerOutput, error) {
	return c.vcdClient.RegisterVmWithLoadBalancer(input)
}

func (c *vcdClient) NsxDescribeLoadBalancers(input *nsx.DescribeLoadBalancersInput) (*nsx.DescribeLoadBalancersOutput, error) {
	return c.vcdClient.NsxDescribeLoadBalancers(input)
}

func (c *vcdClient) NsxDescribeVirtualServer(input *nsx.DescribeVirtualServerInput) (*nsx.DescribeVirtualServerOutput, error) {
	return c.vcdClient.NsxDescribeVirtualServer(input)
}

func (c *vcdClient) NsxRegisterServerPool(input *nsx.RegisterServerPoolInput) (*nsx.RegisterServerPoolOutput, error) {
	return c.vcdClient.RegisterServerPool(input)
}

// NewClient creates our client wrapper object for the actual VCD clients we use.
// For authentication the underlying clients will use either the cluster VCD credentials
// secret if defined (i.e. in the root cluster),
func NewClient(ctrlRuntimeClient client.Client, secretName, namespace, region string) (Client, error) {
	vcdConfig := &vcd.Config{Region: vcd.String(region)}

	if secretName != "" {
		var secret corev1.Secret
		if err := ctrlRuntimeClient.Get(context.Background(), client.ObjectKey{Namespace: namespace, Name: secretName}, &secret); err != nil {
			return nil, err
		}
		user, ok := secret.Data[VcdUserName]
		if !ok {
			return nil, fmt.Errorf("VCD credentials secret %v did not contain key %v",
				secretName, VcdUserName)
		}
		password, ok := secret.Data[VcdPassword]
		if !ok {
			return nil, fmt.Errorf("VCD credentials secret %v did not contain key %v",
				secretName, VcdPassword)
		}
        org, ok := secret.Data[VcdOrg]
        if !ok {
            return nil, fmt.Errorf("VCD org secret %v did not contain key %v",
                secretName, VcdOrg)
        }

		vcdConfig := Config{
                User: user,
                Password: password,
                Org: org,
                Insecure: true,
			}
	}

	// Otherwise default to relying on the IAM role of the masters where the actuator is running:
	vcdClient, err := vcdConfig.Client()
	if err != nil {
		return nil, err
	}
	return &vcdClient, nil
}

// NewClientFromKeys creates our client wrapper object for the actual VCD clients we use.
// For authentication the underlying clients will use VCD credentials.
// Not everything can be AWS
/*
func NewClientFromKeys(accessKey, secretAccessKey, region string) (Client, error) {
	vcdConfig := &vcd.Config{
		Region: vcd.String(region),
		Credentials: credentials.NewStaticCredentials(
			accessKey,
			secretAccessKey,
			"",
		),
	}

	s, err := session.NewSession(vcdConfig)
	if err != nil {
		return nil, err
	}
	s.Handlers.Build.PushBackNamed(addProviderVersionToUserAgent)

	return &vcdClient{
		vcdClient:   vcd.New(s),
		elbClient:   elb.New(s),
		elbv2Client: elbv2.New(s),
	}, nil
}
*/

// addProviderVersionToUserAgent is a named handler that will add cluster-api-provider-vcd
// version information to requests made by the VCD SDK.
var addProviderVersionToUserAgent = request.NamedHandler{
	Name: "openshift.io/cluster-api-provider-vcd",
	Fn:   request.MakeAddToUserAgentHandler("openshift.io cluster-api-provider-vcd", version.Version.String()),
}
